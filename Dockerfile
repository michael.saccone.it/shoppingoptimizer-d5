FROM gradle:jdk16 AS build

ARG env=production
ARG PACKAGES_API_TOKEN=$PACKAGES_API_TOKEN

WORKDIR /src
COPY . .
RUN mv src/main/resources/application.$env.properties src/main/resources/application.properties
RUN gradle build -x test


# ---- Runtime enviroment ----
FROM openjdk:16-slim AS runtime

EXPOSE 8080

COPY --from=build /src/build/libs/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
