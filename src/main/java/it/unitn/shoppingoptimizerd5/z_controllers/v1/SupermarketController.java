package it.unitn.shoppingoptimizerd5.z_controllers.v1;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import it.unitn.shoppingoptimizerd5.services.abstracts.SupermarketService;
import it.unitn.shoppingoptimizerd5.shared.dtos.commands.SuperMarketDto;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.SupermarketQueryDto;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "Gestione Supermercati")
@RestController
@RequestMapping("supermarkets")
public class SupermarketController {

    private final SupermarketService supermarketService;

    public SupermarketController(SupermarketService supermarketService) {
        this.supermarketService = supermarketService;
    }

    @ApiOperation("Creazione supermercato")
    @PostMapping
    public void createSupermarket(@RequestBody SuperMarketDto superMarketDto) {
        supermarketService.createSupermarket(superMarketDto);
    }

    @ApiOperation("Ottenimento di tutti i supermercati")
    @GetMapping
    public List<SupermarketQueryDto> getAllSupermarkets() {
        return supermarketService.getAllSupermarkets();
    }

    @ApiOperation("Eliminazione di un supermercato")
    @DeleteMapping("{id}")
    public void deleteSupermarketById(@PathVariable Integer id) {
        supermarketService.deleteSupermarketById(id);
    }

    @ApiOperation("Modifica di un supermercato")
    @PutMapping("{id}")
    public void editSuperMarketById(@PathVariable Integer id, @RequestBody SuperMarketDto superMarketDto) {
        supermarketService.editSupermarketById(id, superMarketDto);
    }
}
