package it.unitn.shoppingoptimizerd5.z_controllers.v1;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import it.unitn.shoppingoptimizerd5.services.abstracts.CategoryService;
import it.unitn.shoppingoptimizerd5.shared.dtos.commands.CategoryDto;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.CategoryQueryDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "Categorie di prodotto")
@RestController
@RequestMapping("categories")
public class CategoryController {

    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @ApiOperation("Creazione di una categoria")
    @PostMapping
    public void createCategory(@RequestBody CategoryDto categoryDto) {
        categoryService.createCategory(categoryDto);
    }

    @ApiOperation("Ottenimento di tutte le categorie")
    @GetMapping
    public List<CategoryQueryDto> getAllCategories() {
        return categoryService.getAllCategories();
    }

    @ApiOperation("Ottenimento singola categoria")
    @GetMapping("{id}")
    public CategoryQueryDto getCategoryById(@PathVariable Integer id) {
        return categoryService.getCategoryById(id);
    }

    @ApiOperation("Eliminazione categoria")
    @DeleteMapping("{id}")
    public void deleteCategoryById(@PathVariable Integer id) {
        categoryService.deleteCategoryById(id);
    }

    @ApiOperation("Modifica categoria")
    @PutMapping("{id}")
    public void editCategoryById(@PathVariable Integer id, @RequestBody CategoryDto categoryDto) {
        categoryService.editCategoryById(id, categoryDto);
    }

}
