package it.unitn.shoppingoptimizerd5.z_controllers.v1;


import io.swagger.annotations.Api;
import it.unitn.shoppingoptimizerd5.shared.dtos.ErrorDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "test")
@RestController
@RequestMapping("test")
public class TestController {

    @GetMapping
    public ErrorDto testApplication() {
        return new ErrorDto("Hello back! \n  If you're seeing this, it works!");
    }
}
