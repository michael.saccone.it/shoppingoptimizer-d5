package it.unitn.shoppingoptimizerd5.z_controllers.v1;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import it.unitn.shoppingoptimizerd5.services.abstracts.ProductService;
import it.unitn.shoppingoptimizerd5.shared.dtos.commands.ProductDto;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.ProductQueryDto;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "Prodotti")
@RestController
@RequestMapping("products")
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @ApiOperation("Creazione prodotto")
    @PostMapping
    public void createProduct(@RequestBody ProductDto productDto) {
        productService.createProduct(productDto);
    }

    @ApiOperation("Ottenimento di un prodotto da Id")
    @GetMapping("{id}")
    public ProductQueryDto getProductById(@PathVariable Integer id) {
        return productService.getProductById(id);
    }

    @ApiOperation("Ottenimento di tutti i prodotti")
    @GetMapping
    public List<ProductQueryDto> getAllProducts() {
        return productService.getAllProducts();
    }

    @ApiOperation("Ottenimento dei prodotti di una categoria")
    @GetMapping("/category/{categoryId}")
    public List<ProductQueryDto> getCategoryProducts(@PathVariable Integer categoryId) {
        return productService.getCategoryProducts(categoryId);
    }

    @ApiOperation("Eliminazione di un prodotto")
    @DeleteMapping("{id}")
    public void deleteProductById(@PathVariable Integer id) {
        productService.deleteProductById(id);
    }

    @ApiOperation("Modifica di un prodotto")
    @PutMapping("{id}")
    public void editProductById(@PathVariable Integer id, @RequestBody ProductDto productDto) {
        productService.editProductById(id, productDto);
    }

}
