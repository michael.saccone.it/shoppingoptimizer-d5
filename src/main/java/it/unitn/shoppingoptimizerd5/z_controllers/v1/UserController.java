package it.unitn.shoppingoptimizerd5.z_controllers.v1;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import it.unitn.shoppingoptimizerd5.services.abstracts.UserService;
import it.unitn.shoppingoptimizerd5.shared.dtos.commands.UserDto;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.UserQueryDto;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "Gestione utenti")
@RestController
@RequestMapping("users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @ApiOperation("Ottenimento di tutti gli utenti")
    @GetMapping
    public List<UserQueryDto> getAllUsers() {
        return userService.getAllUsers();
    }

    @ApiOperation("Registrazione di un utente")
    @PostMapping
    public void createUser(@RequestBody UserDto userDto) {
        userService.createUser(userDto);
    }

    @ApiOperation("Creazione di un utente amministratore")
    @PostMapping("admin")
    public void createAdminUser(@RequestBody UserDto userDto) {
        userService.createAdminUser(userDto);
    }

    @ApiOperation("Eliminazione di un utente")
    @DeleteMapping("{id}")
    public void deleteUserById(@PathVariable Integer id) {
        userService.deleteUserById(id);
    }

    @ApiOperation("Modifica di un utente")
    @PutMapping("{id}")
    public void editUserById(@PathVariable Integer id, @RequestBody UserDto user) {
        this.userService.editUserById(id, user);
    }
}
