package it.unitn.shoppingoptimizerd5.z_controllers.v1;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import it.unitn.shoppingoptimizerd5.services.abstracts.UserService;
import it.unitn.shoppingoptimizerd5.shared.dtos.commands.AuthenticationDto;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.UserQueryDto;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Autenticazione utenti")
@RestController
@RequestMapping("auth")
public class AuthController {

    public final UserService userService;

    public AuthController(UserService userService) {
        this.userService = userService;
    }

    @ApiOperation("Login - Accesso alla piattaforma ShopnGo")
    @PostMapping("login")
    public UserQueryDto loginUser(@RequestBody AuthenticationDto authenticationDto) {
        return userService.getUserByEmail(authenticationDto);
    }

}
