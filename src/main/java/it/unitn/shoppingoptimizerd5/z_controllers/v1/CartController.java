package it.unitn.shoppingoptimizerd5.z_controllers.v1;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import it.unitn.shoppingoptimizerd5.services.abstracts.CartService;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.ProductQueryDto;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "Gestione carrello")
@RestController
@RequestMapping("cart")
public class CartController {

    private final CartService cartService;

    public CartController(CartService cartService) {
        this.cartService = cartService;
    }

    @ApiOperation("Aggiunge il prodotto specificato al carrello dell'utente loggato")
    @PostMapping("{userId}/{productId}")
    public void addProductToCart(@PathVariable Integer userId, @PathVariable Integer productId) {
        cartService.addProductToUserCart(userId, productId);
    }

    @ApiOperation("Ottiene la lista di prodotti presenti nel carrello dell'utente loggato")
    @GetMapping("{userId}")
    public List<ProductQueryDto> getCartProducts(@PathVariable Integer userId) {
        return cartService.getUserCartProducts(userId);
    }

    @ApiOperation("Aggiunge il prodotto specificato al carrello dell'utente loggato")
    @DeleteMapping("{userId}/{productId}")
    public void removeProductFromUserCart(@PathVariable Integer userId, @PathVariable Integer productId) {
        cartService.removeProductFromUserCart(userId, productId);
    }


}
