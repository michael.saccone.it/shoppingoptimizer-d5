package it.unitn.shoppingoptimizerd5.services.abstracts;

import it.unitn.shoppingoptimizerd5.shared.dtos.queries.ProductQueryDto;

import java.util.List;

public interface CartService {

    void addProductToUserCart(Integer userId, Integer productId);
    List<ProductQueryDto> getUserCartProducts(Integer userId);
    void removeProductFromUserCart(Integer userId, Integer productId);

}
