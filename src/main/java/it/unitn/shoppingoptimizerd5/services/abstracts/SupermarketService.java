package it.unitn.shoppingoptimizerd5.services.abstracts;

import it.unitn.shoppingoptimizerd5.shared.dtos.commands.SuperMarketDto;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.SupermarketQueryDto;

import java.util.List;

public interface SupermarketService {

    public void createSupermarket(SuperMarketDto supermarket);
    public List<SupermarketQueryDto> getAllSupermarkets();
    public void deleteSupermarketById(Integer id);
    public void editSupermarketById(Integer id, SuperMarketDto superMarketDto);

}
