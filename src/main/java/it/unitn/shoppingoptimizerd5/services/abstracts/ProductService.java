package it.unitn.shoppingoptimizerd5.services.abstracts;

import it.unitn.shoppingoptimizerd5.shared.dtos.commands.ProductDto;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.ProductQueryDto;

import java.util.List;

public interface ProductService {

    void createProduct(ProductDto productDto);
    ProductQueryDto getProductById(Integer id);
    List<ProductQueryDto> getAllProducts();
    List<ProductQueryDto> getCategoryProducts(Integer categoryId);
    void editProductById(Integer id, ProductDto productDto);
    void deleteProductById(Integer id);

}
