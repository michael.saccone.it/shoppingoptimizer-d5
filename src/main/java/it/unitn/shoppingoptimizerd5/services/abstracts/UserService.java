package it.unitn.shoppingoptimizerd5.services.abstracts;

import it.unitn.shoppingoptimizerd5.shared.dtos.commands.AuthenticationDto;
import it.unitn.shoppingoptimizerd5.shared.dtos.commands.UserDto;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.UserQueryDto;

import java.util.List;

public interface UserService {

    public void createUser(UserDto user);
    public void createAdminUser(UserDto user);
    public void deleteUserById(Integer id);
    public List<UserQueryDto> getAllUsers();
    public void editUserById(Integer id, UserDto userDto);
    public UserQueryDto getUserByEmail(AuthenticationDto email);

}
