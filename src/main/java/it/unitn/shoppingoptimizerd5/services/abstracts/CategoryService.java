package it.unitn.shoppingoptimizerd5.services.abstracts;


import it.unitn.shoppingoptimizerd5.shared.dtos.commands.CategoryDto;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.CategoryQueryDto;

import java.util.List;

public interface CategoryService {

    void createCategory(CategoryDto categoryDto);
    List<CategoryQueryDto> getAllCategories();
    CategoryQueryDto getCategoryById(Integer categoryId);
    void deleteCategoryById(Integer categoryId);
    void editCategoryById(Integer categoryId, CategoryDto categoryDto);

}
