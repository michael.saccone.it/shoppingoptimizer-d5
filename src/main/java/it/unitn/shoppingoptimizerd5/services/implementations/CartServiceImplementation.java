package it.unitn.shoppingoptimizerd5.services.implementations;

import it.unitn.shoppingoptimizerd5.database.entities.ProductEntity;
import it.unitn.shoppingoptimizerd5.database.repositories.CartRepository;
import it.unitn.shoppingoptimizerd5.database.repositories.ProductRepository;
import it.unitn.shoppingoptimizerd5.database.repositories.UserRepository;
import it.unitn.shoppingoptimizerd5.services.abstracts.CartService;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.ProductQueryDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartServiceImplementation implements CartService {

    private final CartRepository cartRepository;
    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final ModelMapper mapper;

    public CartServiceImplementation(CartRepository cartRepository, UserRepository userRepository, ProductRepository productRepository, ModelMapper mapper) {
        this.cartRepository = cartRepository;
        this.userRepository = userRepository;
        this.productRepository = productRepository;
        this.mapper = mapper;
    }

    @Override
    public void addProductToUserCart(Integer userId, Integer productId) {
        var user = userRepository.findById(userId).orElseThrow();
        var cart = user.getCart();
        var product = productRepository.findById(productId)
                .orElseThrow(() -> new RuntimeException("Prodotto con id " + productId + " non esistente"));
        cart.getProducts().add(product);
        cartRepository.save(cart);
    }

    @Override
    public List<ProductQueryDto> getUserCartProducts(Integer userId) {
        var user = userRepository.findById(userId).orElseThrow();
        var cart = user.getCart();
        var products = cart.getProducts();

        return products.stream().map(this::mapProduct).toList();
    }

    private ProductQueryDto mapProduct(ProductEntity product) {
        return productRepository.findProjectedById(product.getId()).get();
    }

    @Override
    public void removeProductFromUserCart(Integer userId, Integer productId) {
        var user = userRepository.findById(userId).orElseThrow();
        var cart = user.getCart();
        var products = cart.getProducts();
        products.removeIf(product -> product.getId().equals(productId));
        cartRepository.save(cart);
    }
}
