package it.unitn.shoppingoptimizerd5.services.implementations;

import it.unitn.shoppingoptimizerd5.database.entities.PositionEntity;
import it.unitn.shoppingoptimizerd5.database.entities.SupermarketEntity;
import it.unitn.shoppingoptimizerd5.database.repositories.PositionRepository;
import it.unitn.shoppingoptimizerd5.database.repositories.SupermarketRepository;
import it.unitn.shoppingoptimizerd5.services.abstracts.SupermarketService;
import it.unitn.shoppingoptimizerd5.shared.dtos.commands.SuperMarketDto;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.SupermarketQueryDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SupermarketServiceImplementation implements SupermarketService {

    private final PositionRepository positionRepository;
    private final SupermarketRepository supermarketRepository;
    private final ModelMapper mapper;

    @Autowired
    public SupermarketServiceImplementation(PositionRepository positionRepository, SupermarketRepository supermarketRepository, ModelMapper mapper) {
        this.positionRepository = positionRepository;
        this.supermarketRepository = supermarketRepository;
        this.mapper = mapper;
    }

    @Override
    public void createSupermarket(SuperMarketDto supermarket) {
        var positionEntity = mapper.map(supermarket.getPosition(), PositionEntity.class);

        var supermarketEntity = SupermarketEntity.builder()
                .position(positionRepository.save(positionEntity))
                .name(supermarket.getName())
                .build();

        supermarketRepository.save(supermarketEntity);
    }

    @Override
    public List<SupermarketQueryDto> getAllSupermarkets() {
        return supermarketRepository.findAllProjectedBy();
    }

    @Override
    public void deleteSupermarketById(Integer id) {
        if(supermarketRepository.existsById(id))
            supermarketRepository.deleteById(id);
    }

    @Override
    public void editSupermarketById(Integer id, SuperMarketDto superMarketDto) {
        var supermarket = supermarketRepository.findById(id).orElseThrow();
        mapper.map(superMarketDto, supermarket);
        supermarketRepository.saveAndFlush(supermarket);
    }

}
