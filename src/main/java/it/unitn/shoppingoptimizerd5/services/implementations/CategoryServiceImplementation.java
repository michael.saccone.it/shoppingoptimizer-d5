package it.unitn.shoppingoptimizerd5.services.implementations;

import it.unitn.shoppingoptimizerd5.database.entities.ProductCategoryEntity;
import it.unitn.shoppingoptimizerd5.database.repositories.CategoryRepository;
import it.unitn.shoppingoptimizerd5.services.abstracts.CategoryService;
import it.unitn.shoppingoptimizerd5.shared.dtos.commands.CategoryDto;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.CategoryQueryDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImplementation implements CategoryService {
    private final ModelMapper mapper;
    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImplementation(ModelMapper mapper, CategoryRepository categoryRepository) {
        this.mapper = mapper;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public void createCategory(CategoryDto categoryDto) {
        var category = ProductCategoryEntity.builder()
                .name(categoryDto.getName())
                .build();
        categoryRepository.save(category);
    }

    @Override
    public List<CategoryQueryDto> getAllCategories() {
        return categoryRepository.findAllProjectedBy();
    }

    @Override
    public CategoryQueryDto getCategoryById(Integer categoryId) {
        return categoryRepository.findProjectedById(categoryId)
                .orElseThrow(() -> new RuntimeException("Categoria con ID " + categoryId + " non esistente"));
    }

    @Override
    public void deleteCategoryById(Integer categoryId) {
        if(categoryRepository.existsById(categoryId))
            categoryRepository.deleteById(categoryId);
    }

    @Override
    public void editCategoryById(Integer categoryId, CategoryDto categoryDto) {
        var category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new RuntimeException("Categoria con ID \" + categoryId + \" non esistente"));
        category.setName(categoryDto.getName());
        categoryRepository.save(category);
    }
}
