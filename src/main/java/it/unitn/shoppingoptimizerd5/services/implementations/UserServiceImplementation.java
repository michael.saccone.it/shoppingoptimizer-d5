package it.unitn.shoppingoptimizerd5.services.implementations;

import it.unitn.shoppingoptimizerd5.database.entities.CartEntity;
import it.unitn.shoppingoptimizerd5.database.entities.UserEntity;
import it.unitn.shoppingoptimizerd5.database.repositories.UserRepository;
import it.unitn.shoppingoptimizerd5.services.abstracts.UserService;
import it.unitn.shoppingoptimizerd5.shared.dtos.commands.AuthenticationDto;
import it.unitn.shoppingoptimizerd5.shared.dtos.commands.UserDto;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.UserQueryDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImplementation implements UserService {

    private final UserRepository userRepository;
    private final ModelMapper mapper;

    @Autowired
    public UserServiceImplementation(UserRepository userRepository, ModelMapper mapper) {
        this.userRepository = userRepository;
        this.mapper = mapper;
    }


    @Override
    public void createUser(UserDto user) {
        var userEntity = mapper.map(user, UserEntity.class);
        userEntity.setCart(new CartEntity());
        userRepository.save(userEntity);
    }

    @Override
    public void createAdminUser(UserDto user) {
        var userEntity = mapper.map(user, UserEntity.class);
        userEntity.setAdmin(true);
        userRepository.save(userEntity);
    }

    @Override
    public void deleteUserById(Integer id) {
        if(userRepository.existsById(id))
            userRepository.deleteById(id);
        else
            // TODO: restituire errore json
            System.out.println("Utente non esistente! (id: " + id.toString() + ")");
    }

    @Override
    public List<UserQueryDto> getAllUsers() {
        return userRepository.findAllProjectedBy();
    }

    @Override
    public void editUserById(Integer id, UserDto userDto) {
        var user = userRepository.findById(id).orElseThrow();
        mapper.map(userDto, user);
        userRepository.saveAndFlush(user);
    }

    @Override
    public UserQueryDto getUserByEmail(AuthenticationDto authenticationDto) {
        var user = userRepository.findProjectedByEmail(authenticationDto.getEmail())
                .orElseThrow(() -> new RuntimeException("Email non presente"));

        return user;
    }
}
