package it.unitn.shoppingoptimizerd5.services.implementations;

import it.unitn.shoppingoptimizerd5.database.entities.ProductEntity;
import it.unitn.shoppingoptimizerd5.database.repositories.CategoryRepository;
import it.unitn.shoppingoptimizerd5.database.repositories.ProductRepository;
import it.unitn.shoppingoptimizerd5.services.abstracts.ProductService;
import it.unitn.shoppingoptimizerd5.shared.dtos.commands.ProductDto;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.ProductQueryDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImplementation implements ProductService {

    private final ModelMapper mapper;
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    public ProductServiceImplementation(ModelMapper mapper, ProductRepository productRepository, CategoryRepository categoryRepository) {
        this.mapper = mapper;
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public void createProduct(ProductDto productDto) {
        var category = categoryRepository.findById(productDto.getCategoryId())
                .orElseThrow(() -> new RuntimeException("Categoria con id " + productDto.getCategoryId() + " non esistente!"));
        var product = ProductEntity.builder()
                .name(productDto.getName())
                .rating(productDto.getRating())
                .category(category)
                .build();
        productRepository.save(product);
    }

    @Override
    public ProductQueryDto getProductById(Integer id) {
        return productRepository.findProjectedById(id)
                .orElseThrow(() -> new RuntimeException("Prodotto con id " + id + " non esistente"));
    }

    @Override
    public List<ProductQueryDto> getAllProducts() {
        return productRepository.findAllProjectedBy();
    }

    @Override
    public List<ProductQueryDto> getCategoryProducts(Integer categoryId) {
        return productRepository.findAllProjectedByCategory_Id(categoryId);
    }

    @Override
    public void editProductById(Integer id, ProductDto productDto) {
        var product = productRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Non esiste alcun prodotto con id " + id + "!"));

        var category = categoryRepository.findById(productDto.getCategoryId())
                .orElseThrow(() -> new RuntimeException("Non esiste una categoria con id " + productDto.getCategoryId() + "!"));

        mapper.map(productDto, product);
        product.setCategory(category);
        productRepository.save(product);
    }

    @Override
    public void deleteProductById(Integer id) {
        if(productRepository.existsById(id))
            productRepository.deleteById(id);
    }
}
