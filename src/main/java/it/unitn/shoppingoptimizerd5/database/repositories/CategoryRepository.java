package it.unitn.shoppingoptimizerd5.database.repositories;

import it.unitn.shoppingoptimizerd5.database.entities.ProductCategoryEntity;
import it.unitn.shoppingoptimizerd5.shared.dtos.commands.CategoryDto;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.CategoryQueryDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<ProductCategoryEntity, Integer> {

    Optional<CategoryQueryDto> findProjectedById(Integer id);
    List<CategoryQueryDto> findAllProjectedBy();

}
