package it.unitn.shoppingoptimizerd5.database.repositories;

import it.unitn.shoppingoptimizerd5.database.entities.UserEntity;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.UserQueryDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    List<UserQueryDto> findAllProjectedBy();

    Optional<UserEntity> findByEmail(String email);
    Optional<UserQueryDto> findProjectedByEmail(String email);

    Boolean existsByEmail(String email);

    void deleteByEmail(String email);

}
