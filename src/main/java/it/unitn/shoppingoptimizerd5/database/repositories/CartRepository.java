package it.unitn.shoppingoptimizerd5.database.repositories;

import it.unitn.shoppingoptimizerd5.database.entities.CartEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartRepository extends JpaRepository<CartEntity, Integer> {


}
