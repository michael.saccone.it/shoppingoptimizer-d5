package it.unitn.shoppingoptimizerd5.database.repositories;

import it.unitn.shoppingoptimizerd5.database.entities.ProductEntity;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.ProductQueryDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Integer> {

    Optional<ProductQueryDto> findProjectedById(Integer id);
    List<ProductQueryDto> findAllProjectedBy();
    List<ProductQueryDto> findAllProjectedByCategory_Id(Integer categoryId);

}
