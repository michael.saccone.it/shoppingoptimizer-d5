package it.unitn.shoppingoptimizerd5.database.repositories;

import it.unitn.shoppingoptimizerd5.database.entities.PositionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PositionRepository extends JpaRepository<PositionEntity, Integer> {
}
