package it.unitn.shoppingoptimizerd5.database.repositories;

import it.unitn.shoppingoptimizerd5.database.entities.SupermarketEntity;
import it.unitn.shoppingoptimizerd5.shared.dtos.queries.SupermarketQueryDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupermarketRepository extends JpaRepository<SupermarketEntity, Integer> {

    List<SupermarketQueryDto> findAllProjectedBy();

}
