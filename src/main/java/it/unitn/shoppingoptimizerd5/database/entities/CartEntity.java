package it.unitn.shoppingoptimizerd5.database.entities;

import it.unitn.shoppingoptimizerd5.database.abstracts.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "carts")
public class CartEntity extends BaseEntity {
    @ManyToMany
    private List<ProductEntity> products;
}
