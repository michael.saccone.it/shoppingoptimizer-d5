package it.unitn.shoppingoptimizerd5.database.entities;


import it.unitn.shoppingoptimizerd5.database.abstracts.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "supermarkets")
public class SupermarketEntity extends BaseEntity {

    private String name;

    @OneToMany(mappedBy = "supermarket")
    private List<MarketProductEntity> marketProducts;

    @JoinColumn
    @OneToOne(cascade = {javax.persistence.CascadeType.ALL})
    private PositionEntity position;

}
