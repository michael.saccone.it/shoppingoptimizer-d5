package it.unitn.shoppingoptimizerd5.database.entities;

import it.unitn.shoppingoptimizerd5.database.abstracts.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "users")
public class UserEntity extends BaseEntity {

    private String name;
    private String surname;
    @Column(nullable = false, unique = true)
    private String email;

    @Builder.Default
    @Column(nullable = false, updatable = false)
    private Boolean admin = false;

    @ManyToMany
    private List<ProductEntity> favoritesProducts;

    @JoinColumn
    @OneToOne(cascade = {CascadeType.ALL})
    private CartEntity cart;

    @JoinColumn
    @OneToOne(cascade = {CascadeType.ALL})
    private PositionEntity position;

}
