package it.unitn.shoppingoptimizerd5.database.entities;

import it.unitn.shoppingoptimizerd5.database.abstracts.BaseEntity;
import lombok.*;

import javax.persistence.Entity;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "positions")
public class PositionEntity extends BaseEntity {

    private Double latitude;
    private Double longitude;

}
