package it.unitn.shoppingoptimizerd5.database.entities;

import it.unitn.shoppingoptimizerd5.database.abstracts.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "products")
public class ProductEntity extends BaseEntity {
    private String name;
    private Integer rating;

    @JoinColumn(nullable = false)
    @ManyToOne(optional = false)
    private ProductCategoryEntity category;

    @OneToMany(mappedBy = "product")
    private List<MarketProductEntity> marketProducts;

    @ManyToMany(mappedBy = "favoritesProducts")
    private List<UserEntity> preferredBy;

    @ManyToMany(mappedBy = "products")
    private List<CartEntity> carts;

}
