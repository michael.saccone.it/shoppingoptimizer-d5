package it.unitn.shoppingoptimizerd5.database.entities;

import it.unitn.shoppingoptimizerd5.database.abstracts.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "categories")
public class ProductCategoryEntity extends BaseEntity {

    private String name;

    @OneToMany(mappedBy = "category", cascade = javax.persistence.CascadeType.REMOVE)
    private List<ProductEntity> products;

}
