package it.unitn.shoppingoptimizerd5.database.entities;

import it.unitn.shoppingoptimizerd5.database.abstracts.BaseEntity;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;


/*
* Entita' di collegamento tra Supermarket e Product.
* Ogni supermercato puo' vendere il medesimo prodotto in quantita' e prezzo diverso
* */

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "market_products")
public class MarketProductEntity extends BaseEntity {

    @ManyToOne
    private SupermarketEntity supermarket;
    @ManyToOne
    private ProductEntity product;
    private Integer quantity;
    private Double price;

}
