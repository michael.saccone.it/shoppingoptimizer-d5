package it.unitn.shoppingoptimizerd5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShoppingoptimizerD5Application {

    public static void main(String[] args) {
        SpringApplication.run(ShoppingoptimizerD5Application.class, args);
    }

}
