package it.unitn.shoppingoptimizerd5.shared.dtos.queries;

public interface ProductQueryDto {

    Integer getId();
    String getName();
    Integer getRating();

    CategoryQueryDto getCategory();

}
