package it.unitn.shoppingoptimizerd5.shared.dtos.queries;

public interface PositionQueryDto {

    public Double getLatitude();
    public Double getLongitude();

}
