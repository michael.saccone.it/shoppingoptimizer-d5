package it.unitn.shoppingoptimizerd5.shared.dtos.commands;

import lombok.Value;

@Value
public class ProductDto {

    String name;
    Integer rating;
    Integer categoryId;

}
