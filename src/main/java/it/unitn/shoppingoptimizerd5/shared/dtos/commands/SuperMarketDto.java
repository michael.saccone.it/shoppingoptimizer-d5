package it.unitn.shoppingoptimizerd5.shared.dtos.commands;

import lombok.Value;

@Value
public class SuperMarketDto {

    String name;

    @org.springframework.beans.factory.annotation.Value("#{target.position}")
    PositionDto position;

}
