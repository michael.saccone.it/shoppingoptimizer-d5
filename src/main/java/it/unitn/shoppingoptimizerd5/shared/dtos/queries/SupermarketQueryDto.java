package it.unitn.shoppingoptimizerd5.shared.dtos.queries;

public interface SupermarketQueryDto {

    public Integer getId();

    public String getName();

    public PositionQueryDto getPosition();

}
