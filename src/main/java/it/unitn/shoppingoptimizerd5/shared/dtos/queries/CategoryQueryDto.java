package it.unitn.shoppingoptimizerd5.shared.dtos.queries;

public interface CategoryQueryDto {
    Integer getId();
    String getName();
}
