package it.unitn.shoppingoptimizerd5.shared.dtos.commands;

import lombok.Value;

@Value
public class PositionDto {

    Double latitude;
    Double longitude;

}
