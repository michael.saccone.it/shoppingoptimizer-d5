package it.unitn.shoppingoptimizerd5.shared.dtos.commands;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDto {

    String name;
    String surname;
    String email;

}
