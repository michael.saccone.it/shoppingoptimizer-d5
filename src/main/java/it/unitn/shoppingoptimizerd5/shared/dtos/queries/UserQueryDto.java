package it.unitn.shoppingoptimizerd5.shared.dtos.queries;

public interface UserQueryDto {

    public Integer getId();
    public String getEmail();
    public String getName();
    public String getSurname();
    public Boolean getAdmin();

}
