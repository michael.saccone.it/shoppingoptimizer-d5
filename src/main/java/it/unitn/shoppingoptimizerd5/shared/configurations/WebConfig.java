package it.unitn.shoppingoptimizerd5.shared.configurations;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.HandlerTypePredicate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    public static String[] allowedCorsOrigin = {
            "http://localhost:8080",
            "https://shoptimizer.michaelsaccone.it",
            "https://g17.michaelsaccone.it",
            "https://shopngo.michaelsaccone.it"
    };

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry
                .addMapping("/**")
                .allowedOrigins(allowedCorsOrigin)
                .allowedMethods("*");
    }

    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer
                .addPathPrefix("api/v1", HandlerTypePredicate.forBasePackage("it.unitn.shoppingoptimizerd5.z_controllers.v1")
                        .and(HandlerTypePredicate.forAnnotation(RestController.class))
                );
    }

}
