package it.unitn.shoppingoptimizerd5.shared.configurations;

import it.unitn.shoppingoptimizerd5.database.repositories.UserRepository;
import it.unitn.shoppingoptimizerd5.services.abstracts.UserService;
import it.unitn.shoppingoptimizerd5.shared.dtos.commands.UserDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class DataInit implements CommandLineRunner {

    private final UserService userService;
    private final UserRepository userRepository;

    public DataInit(UserService userService, UserRepository userRepository) {
        this.userService = userService;
        this.userRepository = userRepository;
    }

    //  codice da eseguire all'avvio dell'applicazione
    @Override
    public void run(String... args) throws Exception {
        System.out.println("DataInit :: Starting Application Engines");
        initUsers();
    }

    private void initUsers() {
        createUserIfNotExisting(UserDto.builder()
                .email("admin@shop.it")
                .name("System")
                .surname("Admin")
                .build(), true);

        createUserIfNotExisting(UserDto.builder()
                .email("user@shop.it")
                .name("User")
                .surname("Base")
                .build(), false);
    }

    private void createUserIfNotExisting(UserDto user, boolean admin) {
        if(!userRepository.existsByEmail(user.getEmail())) {
            log.info("Utente " + user.getName() + " non esistente . . .");
            if (admin) {
                userService.createAdminUser(user);
            } else {
                userService.createUser(user);
            }
            log.info("Creato!");
        }
    }
}
