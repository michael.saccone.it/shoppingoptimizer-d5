package it.unitn.shoppingoptimizerd5.shared.configurations;

import it.unitn.shoppingoptimizerd5.shared.dtos.ErrorDto;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import springfox.documentation.builders.ExampleBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Response;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
//    private final TypeResolver typeResolver;
//
//    public SwaggerConfig(TypeResolver typeResolver) {
//        this.typeResolver = typeResolver;
//    }

    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("it.unitn.shoppingoptimizerd5.z_controllers"))
                .paths(PathSelectors.any())
                .build()
                .globalResponses(HttpMethod.GET, createDefaultErrorResponses())
                .globalResponses(HttpMethod.POST, createDefaultErrorResponses())
                .globalResponses(HttpMethod.PUT, createDefaultErrorResponses())
                .globalResponses(HttpMethod.DELETE, createDefaultErrorResponses())
                .pathMapping("/")
                .enableUrlTemplating(false)
//                .alternateTypeRules(
//                        newRule(typeResolver.resolve(CompletableFuture.class, WildcardType.class),
//                                typeResolver.resolve(WildcardType.class)))
                .apiInfo(apiInfo());
    }

    private List<Response> createDefaultErrorResponses() {
        return List.of(
                createErrorResponse("400", "Errore generato dal client Frontend"),
                createErrorResponse("500", "Errore generato dal server Backend - contattare l'admin")
        );
    }

    private Response createErrorResponse(String code, String description) {
        return new Response(
                code,
                description,
                true,
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.singletonList(new ExampleBuilder()
                        .id("id")
                        .mediaType("*/*")
                        .description("Oggetto JSON con descrizione dell'errore generato")
                        .value(new ErrorDto("messaggio di errore"))
                        .build()),
                Collections.emptyList()
        );
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Shop&Go Api",
                "Sistema di gestione ottimizzata della spesa",
                "1.0",
                "G17 terms of service",
                new Contact(
                        "G17: Michael Saccone, Andy Ditu & Sara Soprana",
                        "",
                        "michael.saccone@studenti.unitn.it"
                ),
                "Trade secret",
                "",
                Collections.emptyList()
        );
    }
}
